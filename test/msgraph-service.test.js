'use strict'

const amqp = require('amqplib')

let _app = null
let _channel = null
let _conn = null
// let conf = {
//   'entitySchema': 'users',
//   'odataMethod' : '$select=id,displayName,mail,accountEnabled&$filter= accountEnabled eq true&$top=999',
//   'apiVersion' : 'beta'
// }

describe('MSGraph Service', function () {
  // process.env.OUTPUT_PIPES = 'Op1,Op2'
  // process.env.LOGGERS = 'logger1,logger2'
  // process.env.EXCEPTION_LOGGERS = 'exlogger1,exlogger2'
  // process.env.BROKER = 'amqp://guest:guest@127.0.0.1/'
  // process.env.INPUT_PIPE = 'demo.pipe.service'
  // process.env.CONFIG = JSON.stringify(conf)
  // process.env.OUTPUT_SCHEME = 'RESULT'
  // process.env.OUTPUT_NAMESPACE = 'RESULT'
  // process.env.ACCOUNT = 'demo account'

  let BROKER = process.env.BROKER
  let INPUT_PIPE = process.env.INPUT_PIPE

  before('init', () => {
    amqp.connect(BROKER)
      .then((conn) => {
        _conn = conn
        return conn.createChannel()
      }).then((channel) => {
        _channel = channel
      }).catch((err) => {
        console.log(err)
      })
  })

  after('terminate child process', function (done) {
    _conn.close()
    done()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(8000)
      _app = require('../app')
      _app.once('init', done)
    })
  })

  describe('#data', () => {
    it('should process the data and send back a result', function (done) {
      this.timeout(11000)
      // sample data
      var auth = { 
        filter: true,
        'token_type': 'Bearer',
        'scope': 'Calendars.Read Calendars.Read.Shared Calendars.ReadWrite Calendars.ReadWrite.Shared Contacts.Read Contacts.Read.Shared Contacts.ReadWrite Contacts.ReadWrite.Shared Device.Command Device.Read DeviceManagementApps.Read.All DeviceManagementApps.ReadWrite.All DeviceManagementConfiguration.Read.All DeviceManagementConfiguration.ReadWrite.All DeviceManagementManagedDevices.PrivilegedOperations.All DeviceManagementManagedDevices.Read.All DeviceManagementManagedDevices.ReadWrite.All DeviceManagementRBAC.Read.All DeviceManagementRBAC.ReadWrite.All DeviceManagementServiceConfiguration.Read.All DeviceManagementServiceConfiguration.ReadWrite.All Directory.AccessAsUser.All Directory.Read.All Directory.ReadWrite.All email Files.Read Files.Read.All Files.Read.Selected Files.ReadWrite Files.ReadWrite.All Files.ReadWrite.AppFolder Files.ReadWrite.Selected Group.Read.All Group.ReadWrite.All IdentityRiskEvent.Read.All Mail.Read Mail.Read.Shared Mail.ReadWrite Mail.ReadWrite.Shared Mail.Send Mail.Send.Shared MailboxSettings.Read MailboxSettings.ReadWrite Notes.Create Notes.Read Notes.Read.All Notes.ReadWrite Notes.ReadWrite.All Notes.ReadWrite.CreatedByApp offline_access openid People.Read profile Reports.Read.All Sites.Read.All Sites.ReadWrite.All Tasks.Read Tasks.Read.Shared Tasks.ReadWrite Tasks.ReadWrite.Shared User.Invite.All User.Read User.Read.All User.ReadBasic.All User.ReadWrite User.ReadWrite.All',
        'expires_in': '3599',
        'ext_expires_in': '0',
        'expires_on': '1516086126',
        'not_before': '1516082226',
        'resource': 'https://graph.microsoft.com',
        'access_token': 'eyJ0eXAiOiJKV1QiLCJub25jZSI6IkFRQUJBQUFBQUFCSGg0a21TX2FLVDVYcmp6eFJBdEh6cjRkR2pWRTU5eFNZTHJqT1hJRFVZSGZhal9sYW1GWTBQbzA5Q3VlYlAtN3F4NkU2TTJqS0loWFZtMF8zZS1FQkFrS3JfR19scTFOWXJDbGZyaVlCUVNBQSIsImFsZyI6IlJTMjU2IiwieDV0IjoiejQ0d01kSHU4d0tzdW1yYmZhSzk4cXhzNVlJIiwia2lkIjoiejQ0d01kSHU4d0tzdW1yYmZhSzk4cXhzNVlJIn0.eyJhdWQiOiJodHRwczovL2dyYXBoLm1pY3Jvc29mdC5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC81MDk4NDMwYy1jMjEyLTQxMWItODcyNS1jMjUxMGFhYmQxZDUvIiwiaWF0IjoxNTE2MDgyMjI2LCJuYmYiOjE1MTYwODIyMjYsImV4cCI6MTUxNjA4NjEyNiwiYWNyIjoiMSIsImFpbyI6IlkyTmdZTmdReXVHaUxuWW1VM1NIN2VLckFUeTdZMzRxWjcvUVBac2RLTUlUcUcyK2RBMEEiLCJhbXIiOlsicHdkIl0sImFwcF9kaXNwbGF5bmFtZSI6InRpbWVrZWVwIiwiYXBwaWQiOiJkOTJjYmQ3OC00ZjdiLTQ0MWYtOTE2Ni05NjI1M2I0ZmI0YTQiLCJhcHBpZGFjciI6IjEiLCJmYW1pbHlfbmFtZSI6IkNSTSIsImdpdmVuX25hbWUiOiJCYXJoZWFkIiwiaXBhZGRyIjoiMTQuMTAyLjE2OC4yNiIsIm5hbWUiOiJCYXJoZWFkIENSTSIsIm9pZCI6IjUyNDdjYzNiLTg5NWEtNDdlYi04Y2UyLWEyZTAxZGEyOTJjYiIsInBsYXRmIjoiMTQiLCJwdWlkIjoiMTAwMzNGRkZBMDVBNEIwRiIsInNjcCI6IkNhbGVuZGFycy5SZWFkIENhbGVuZGFycy5SZWFkLlNoYXJlZCBDYWxlbmRhcnMuUmVhZFdyaXRlIENhbGVuZGFycy5SZWFkV3JpdGUuU2hhcmVkIENvbnRhY3RzLlJlYWQgQ29udGFjdHMuUmVhZC5TaGFyZWQgQ29udGFjdHMuUmVhZFdyaXRlIENvbnRhY3RzLlJlYWRXcml0ZS5TaGFyZWQgRGV2aWNlLkNvbW1hbmQgRGV2aWNlLlJlYWQgRGV2aWNlTWFuYWdlbWVudEFwcHMuUmVhZC5BbGwgRGV2aWNlTWFuYWdlbWVudEFwcHMuUmVhZFdyaXRlLkFsbCBEZXZpY2VNYW5hZ2VtZW50Q29uZmlndXJhdGlvbi5SZWFkLkFsbCBEZXZpY2VNYW5hZ2VtZW50Q29uZmlndXJhdGlvbi5SZWFkV3JpdGUuQWxsIERldmljZU1hbmFnZW1lbnRNYW5hZ2VkRGV2aWNlcy5Qcml2aWxlZ2VkT3BlcmF0aW9ucy5BbGwgRGV2aWNlTWFuYWdlbWVudE1hbmFnZWREZXZpY2VzLlJlYWQuQWxsIERldmljZU1hbmFnZW1lbnRNYW5hZ2VkRGV2aWNlcy5SZWFkV3JpdGUuQWxsIERldmljZU1hbmFnZW1lbnRSQkFDLlJlYWQuQWxsIERldmljZU1hbmFnZW1lbnRSQkFDLlJlYWRXcml0ZS5BbGwgRGV2aWNlTWFuYWdlbWVudFNlcnZpY2VDb25maWd1cmF0aW9uLlJlYWQuQWxsIERldmljZU1hbmFnZW1lbnRTZXJ2aWNlQ29uZmlndXJhdGlvbi5SZWFkV3JpdGUuQWxsIERpcmVjdG9yeS5BY2Nlc3NBc1VzZXIuQWxsIERpcmVjdG9yeS5SZWFkLkFsbCBEaXJlY3RvcnkuUmVhZFdyaXRlLkFsbCBlbWFpbCBGaWxlcy5SZWFkIEZpbGVzLlJlYWQuQWxsIEZpbGVzLlJlYWQuU2VsZWN0ZWQgRmlsZXMuUmVhZFdyaXRlIEZpbGVzLlJlYWRXcml0ZS5BbGwgRmlsZXMuUmVhZFdyaXRlLkFwcEZvbGRlciBGaWxlcy5SZWFkV3JpdGUuU2VsZWN0ZWQgR3JvdXAuUmVhZC5BbGwgR3JvdXAuUmVhZFdyaXRlLkFsbCBJZGVudGl0eVJpc2tFdmVudC5SZWFkLkFsbCBNYWlsLlJlYWQgTWFpbC5SZWFkLlNoYXJlZCBNYWlsLlJlYWRXcml0ZSBNYWlsLlJlYWRXcml0ZS5TaGFyZWQgTWFpbC5TZW5kIE1haWwuU2VuZC5TaGFyZWQgTWFpbGJveFNldHRpbmdzLlJlYWQgTWFpbGJveFNldHRpbmdzLlJlYWRXcml0ZSBOb3Rlcy5DcmVhdGUgTm90ZXMuUmVhZCBOb3Rlcy5SZWFkLkFsbCBOb3Rlcy5SZWFkV3JpdGUgTm90ZXMuUmVhZFdyaXRlLkFsbCBOb3Rlcy5SZWFkV3JpdGUuQ3JlYXRlZEJ5QXBwIG9mZmxpbmVfYWNjZXNzIG9wZW5pZCBQZW9wbGUuUmVhZCBwcm9maWxlIFJlcG9ydHMuUmVhZC5BbGwgU2l0ZXMuUmVhZC5BbGwgU2l0ZXMuUmVhZFdyaXRlLkFsbCBUYXNrcy5SZWFkIFRhc2tzLlJlYWQuU2hhcmVkIFRhc2tzLlJlYWRXcml0ZSBUYXNrcy5SZWFkV3JpdGUuU2hhcmVkIFVzZXIuSW52aXRlLkFsbCBVc2VyLlJlYWQgVXNlci5SZWFkLkFsbCBVc2VyLlJlYWRCYXNpYy5BbGwgVXNlci5SZWFkV3JpdGUgVXNlci5SZWFkV3JpdGUuQWxsIiwic3ViIjoiYlN4a1JEUkRBUWJOSzNvM0hXVWVuZFp5cFhKS25KQW9tOFo2RjlYSDVjMCIsInRpZCI6IjUwOTg0MzBjLWMyMTItNDExYi04NzI1LWMyNTEwYWFiZDFkNSIsInVuaXF1ZV9uYW1lIjoiYXBpQGJhcmhlYWQuY29tIiwidXBuIjoiYXBpQGJhcmhlYWQuY29tIiwidXRpIjoidk04Y0FLQzBlVXlwZElWZHRUWXhBQSIsInZlciI6IjEuMCJ9.mFCHMLj47ZSh2XUKQ5O1wtKE8geEjU1QVcoP1Alpu75wYbHYY5kSkojyP3b3jy2IhwtwlvSzxvl9A0XnbiVMek4n8Z_KpaMH-Y4Z7v5S3RLmNE1-BMprqLwN8wSLoVJoCl5NaHwZffzWLZ5IE8nvhhZYiPjSRucXFwvmkwLieGU6xBTu0sLFusD9H12waNizuW9BbP4ISbQEynWKYxTOiezmftmcZ209lxV4KhxZ32MjS4uNcISqxDVNjz2jXw5jRvIIHHw-jXwI6OSib4kdB35WDr-fGtOn7X2zI_CrPE9Ko7K95RRGiNZVKFPmKFoRxJT5f0P5uEUy1cBa2_o36Q',
        'refresh_token': 'AQABAAAAAABHh4kmS_aKT5XrjzxRAtHzK9u-LWbDuSy1mxkPGMG_xj1jc-y_8Qhln8Ebr19SdxMzacVPSH9UVtSZLD6zl3-Y7L4zw7WUcFY6aRn9zZM9zfB33GxprKiV0TQRBxEvW34X0H2yXNjMsW0U8EjXvHUmbdIbwoPHtvepyiMe5tAaPtg6h9mZWGNgnfy2SrGAP0HVRLvvPJw36LWXBGc7OEcjmPwHNoGyH13mNE05IG8qXJ4AxB1ThNT9AY1Nim9YB_V0Fc48itHOGRMXjdAaY_R_x96s_0uIV1bkNcp7Z4bSw7RsHFgtE6wFaVHqGZNZjlSLPE-VVWflafshD-LepSAdJgvQZMNdpLPQYr0hIiuiEETovBm79BXWSo_z_6TbRy_xBetwONOg6tB4w_zBnLhL3OW3CL6XXVVNQiZ8weP3eihpw6RZXvmJJIC978zodb5vAcYft3Es0DDhe1y6HbBizEUsFuMgrcnGTOOx3vf19yXpoJwP9PD9zCqNntFTZYjRRl1XT7tl6R1YgH3R8CQ5dDGnSaePTELe8wvrl82wGXQq9BaGsSHcQ-cgzXkI_fiPaRNetjV32vRxwBoA09crz_kYHPeia_tznNr2ZU8Dxe82Ps20Z9aumDKDRuytmDqE3j_5VlyL65yrOY7VW9WwC2T8lomwatwtAEK916kAxryG8LQ60U4Osrn3ln-mSlFr10ya8p1MOntthtjVmBHSutMtG3lQpdgTfgh_xXiGWCAA' }

      _channel.sendToQueue(INPUT_PIPE, new Buffer(JSON.stringify(auth)))

      setTimeout(done, 10000)
    })
  })
})
