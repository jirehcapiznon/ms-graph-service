FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/ms-graph-service

WORKDIR /home/node/ms-graph-service

# RUN npm install pm2@2.6.1 -g

# CMD pm2-docker --json app.yml
CMD ["node", "app.js"]